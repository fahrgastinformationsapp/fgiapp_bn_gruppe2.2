package kaph1022.info3.fgiapp_bn_gruppe22.ui.Mapview;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProviders;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import kaph1022.info3.fgiapp_bn_gruppe22.MainActivity;
import kaph1022.info3.fgiapp_bn_gruppe22.R;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Search.Fahrten;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Search.FahrtenListAdapter;


public class MapViewFragment extends Fragment implements OnMapReadyCallback {

    private MapViewViewModel MapViewViewModel;
    private Context mContext;
    private JSONArray details;
    GoogleMap map;
    MapView mapView;
    /*private LatLngBounds Karlsruhe = new LatLngBounds(
                new LatLng(49, 8), new LatLng(48.979, 8.478));*/

    private static final ArrayList<Fahrten> Verbindungsdetails = new ArrayList<Fahrten>() {

    };


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        mContext = getActivity().getApplicationContext();
        ListView Detailfahrt = (ListView) root.findViewById(R.id.ListView_DetailFahrt);

        try {
            details = ((MainActivity) getActivity()).getDetails();
            Verbindungsdetails.clear();

            try {
                for (int i = 0; i < details.length(); i++) {
                    JSONObject leg = details.getJSONObject(i);
                    String transportation = leg.getJSONObject("transportation").getJSONObject("product").getString("name");
                    if (transportation.equals("footpath")){
                        transportation = "Zu Fuß";
                    }
                    try {
                        transportation = leg.getJSONObject("transportation").getString("name");

                    } catch (JSONException e) {
                    }

                    Verbindungsdetails.add(new Fahrten(
                            leg.getJSONObject("origin").getJSONObject("parent").getString("name"),
                            leg.getJSONObject("origin").getString("departureTimeEstimated"),
                            transportation,
                            leg.getJSONObject("destination").getJSONObject("parent").getString("name"),
                            leg.getJSONObject("destination").getString("arrivalTimeEstimated")
                    ));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            details = new JSONArray();
        }
        //MapViewViewModel =
        // ViewModelProviders.of(this).get(MapViewViewModel.class);


        FahrtenListAdapter adapter = new FahrtenListAdapter(mContext, R.layout.adapter_list_view, Verbindungsdetails);
        Detailfahrt.setAdapter(adapter);


        mapView = root.findViewById(R.id.mapView);


        mapView.getMapAsync(this);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }


        return root;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        map = googleMap;
        int[] colors = {Color.RED, Color.BLUE, Color.GREEN};

        Double xCenter = 49d;
        Double yCenter = 8.4;
        Double Distance = 1d;

        try {

            for (int i = 0; i < details.length(); i++) {
                PolylineOptions polyline = new PolylineOptions();

                try {
                    JSONArray coords = details.getJSONObject(i).getJSONArray("coords");

                    for (int j = 0; j < coords.length() - 1; j++) {
                        JSONArray p = coords.getJSONArray(j);
                        polyline.add(new LatLng(p.getDouble(0), p.getDouble(1)));
                    }

                } catch (JSONException e) {
                }

                map.addPolyline(polyline.width(5).color(colors[i % (colors.length)]));
            }
            try {
                int coordsLength = details.getJSONObject(details.length() - 1).getJSONArray("coords").length() - 1;
                Double[] xCoords = {
                        details.getJSONObject(0).getJSONArray("coords").getJSONArray(0).getDouble(0),
                        details.getJSONObject(details.length() - 1).getJSONArray("coords").getJSONArray(coordsLength).getDouble(0)
                };
                Double[] yCoords = {
                        details.getJSONObject(0).getJSONArray("coords").getJSONArray(0).getDouble(1),
                        details.getJSONObject(details.length() - 1).getJSONArray("coords").getJSONArray(coordsLength).getDouble(1)
                };
                Arrays.sort(xCoords);
                Arrays.sort(yCoords);
                xCenter = xCoords[0] + (0.5 * (xCoords[1] - xCoords[0]));
                yCenter = yCoords[0] + (0.5 * (yCoords[1] - yCoords[0]));

                //Pythagoras
                Distance = Math.sqrt(Math.pow(xCoords[1] - xCoords[0], 2) + Math.pow(yCoords[1] - yCoords[0], 2));
                Log.e("fehler", Distance.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(xCenter, yCenter), 14));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        //mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    public void onBackPressed() {

    }
}