package kaph1022.info3.fgiapp_bn_gruppe22.ui.Search;

import android.util.Log;

import org.json.JSONArray;

public class Fahrten {
    private String Starthalte;
    private String StartUhrzeit;
    private String Verkehrsmittel;
    private String Endhalte;
    private String EndUhrzeit;
    private JSONArray legs;

    public Fahrten(String starthalte, String startUhrzeit, String verkehrsmittel, String endhalte, String endUhrzeit, JSONArray legs) {
        Starthalte = starthalte;
        StartUhrzeit = startUhrzeit;
        Verkehrsmittel = verkehrsmittel;
        Endhalte = endhalte;
        EndUhrzeit = endUhrzeit;
        this.legs = legs;
        //Log.e("fehler", Starthalte+ StartUhrzeit + Verkehrsmittel + Endhalte + EndUhrzeit);
    }

    public Fahrten(String starthalte, String startUhrzeit, String verkehrsmittel, String endhalte, String endUhrzeit) {
        Starthalte = starthalte;
        StartUhrzeit = startUhrzeit;
        Verkehrsmittel = verkehrsmittel;
        Endhalte = endhalte;
        EndUhrzeit = endUhrzeit;
        this.legs = new JSONArray();
    }

    public String getStarthalte() {
        return Starthalte;
    }

    public void setStarthalte(String starthalte) {
        Starthalte = starthalte;
    }

    public String getStartUhrzeit() {
        return StartUhrzeit;
    }

    public void setStartUhrzeit(String startUhrzeit) {
        StartUhrzeit = startUhrzeit;
    }

    public String getVerkehrsmittel() {
        return Verkehrsmittel;
    }

    public void setVerkehrsmittel(String verkehrsmittel) {
        Verkehrsmittel = verkehrsmittel;
    }

    public String getEndhalte() {
        return Endhalte;
    }

    public void setEndhalte(String endhalte) {
        Endhalte = endhalte;
    }

    public String getEndUhrzeit() {
        return EndUhrzeit;
    }

    public void setEndUhrzeit(String endUhrzeit) {
        EndUhrzeit = endUhrzeit;
    }

    public JSONArray getLegs() { return legs; }
}
