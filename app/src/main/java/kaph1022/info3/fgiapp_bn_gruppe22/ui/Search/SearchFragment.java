package kaph1022.info3.fgiapp_bn_gruppe22.ui.Search;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import kaph1022.info3.fgiapp_bn_gruppe22.MainActivity;
import kaph1022.info3.fgiapp_bn_gruppe22.R;
import kaph1022.info3.fgiapp_bn_gruppe22.ui.Mapview.MapViewFragment;


public class SearchFragment extends Fragment {

    private Context mContext;
    private SearchViewModel SearchViewModel;
    //url bauen für HaSte
    final String stationURL = "http://smartmmi.demo.mentz.net/smartmmi/XML_STOPFINDER_REQUEST?outputFormat=rapidJson&type_sf=any&name_sf=";



    private static final List<String> Starthaltestelle = new ArrayList<String>();
    private static final List<String> Endhaltestelle = new ArrayList<String>();
    private static final ArrayList<Fahrten> Verbindungen = new ArrayList<Fahrten>();

    private void attachStationListener(final AutoCompleteTextView Textfeld, final List<String> Haltestelle) {
        Textfeld.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String urlStart = stationURL + Textfeld.getText().toString().replaceAll(" ", "%20");

                //Haltestelle.clear();
                RequestQueue urlAnfrage = Volley.newRequestQueue(getContext());


                JsonObjectRequest objectRequest = new JsonObjectRequest(
                        Request.Method.GET,
                        urlStart,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Haltestelle.clear();
                                    JSONArray location = response.getJSONArray("locations");
                                    for (int j = 0; j < location.length(); j++) {
                                        JSONObject Station = location.getJSONObject(j);
                                        String stationName = Station.getString("name");
                                        Haltestelle.add(stationName);
                                    }

                                    Log.e("fehler", Haltestelle.toString());
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                            android.R.layout.simple_dropdown_item_1line, Haltestelle);
                                    Textfeld.setAdapter(adapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("fehler", "Server error bei Haltestellensuche");
                            }
                        });

                urlAnfrage.add(objectRequest);

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SearchViewModel =
                ViewModelProviders.of(this).get(SearchViewModel.class);
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        final AutoCompleteTextView Startpunkt = (AutoCompleteTextView) root.findViewById(R.id.ACTextView_Start);
        final AutoCompleteTextView Zielpunkt = (AutoCompleteTextView) root.findViewById(R.id.ACTextView_Ziel);
        final ListView Routenvorschlaege = (ListView) root.findViewById(R.id.ListView_Routenadapter);
        mContext = getActivity().getApplicationContext();






        this.attachStationListener(Startpunkt, Starthaltestelle);
        this.attachStationListener(Zielpunkt, Endhaltestelle);


        Button Verbindungsuchen = (Button) root.findViewById(R.id.button_search);
        Verbindungsuchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verbindungen.clear();

                //url losschicken, Ergebnisse in Adapter schreiben
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                //final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
                final SimpleDateFormat gewolltesFormat = new SimpleDateFormat("HH:mm");
                gewolltesFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


                final String urlstandardTeil = "http://smartmmi.demo.mentz.net/smartmmi/XML_TRIP_REQUEST2?outputFormat=rapidJson&type_sf=any&type_origin=stop&coordOutputFormat=WGS84[DD.DDDDD]&name_origin=";
                final String urlZwischenStueck = "&type_destination=stop&name_destination=";
                final String Start = Startpunkt.getText().toString().replaceAll(" ", "%20");
                final String Ziel = Zielpunkt.getText().toString().replaceAll(" ", "%20");
                final String urlRoute = urlstandardTeil + Start + urlZwischenStueck + Ziel;

                Log.e("fehler", urlRoute);


                RequestQueue urlAnfrage = Volley.newRequestQueue(getContext());


                JsonObjectRequest objectRequest = new JsonObjectRequest(

                        Request.Method.GET,
                        urlRoute,
                        null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    Log.e("fehler", "Antwort kommt");
                                    JSONArray routen = response.getJSONArray("journeys");

                                    for (int j = 0; j < routen.length(); j++) {
                                        JSONObject journey = routen.getJSONObject(j);
                                        JSONArray legs = journey.getJSONArray("legs");
                                        JSONObject origin = legs.getJSONObject(0).getJSONObject("origin");
                                        JSONObject destination = legs.getJSONObject(legs.length() - 1).getJSONObject("destination");
                                        String linien = "";

                                        for (int k = 0; k < legs.length(); k++) {
                                            try {
                                                String linie = legs.getJSONObject(k).getJSONObject("transportation").getString("number");
                                                if (!linien.contains(linie)) {
                                                    if (linien.length() > 0) {
                                                        linien = linien + ", ";
                                                    }
                                                    linien = linien + linie;
                                                }

                                            } catch (JSONException e) {
                                                //Falls Zwischenetappe Fußweg
                                            }

                                        }

                                        Fahrten verbindung = new Fahrten(
                                                origin.getJSONObject("parent").getString("name"),
                                                origin.getString("departureTimeEstimated"),
                                                linien,
                                                destination.getJSONObject("parent").getString("name"),
                                                destination.getString("arrivalTimeEstimated"),
                                                legs);

                                        Verbindungen.add(verbindung);



                                    }

                                    FahrtenListAdapter adapter = new FahrtenListAdapter(mContext, R.layout.adapter_list_view, Verbindungen);
                                    Routenvorschlaege.setAdapter(adapter);


                                } catch (JSONException e) {
                                    Log.e("fehler", "alles kaputt :(");
                                    e.printStackTrace();

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("fehler", "Server müde, Server 500");
                                Log.e("fehler", error.toString());

                            }
                        }
                );

                objectRequest.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                urlAnfrage.add(objectRequest);


            }


        });

        Routenvorschlaege.setOnItemClickListener(new ListView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fahrten verbindung = (Fahrten) Routenvorschlaege.getItemAtPosition(position);
                MapViewFragment mv = new MapViewFragment();
                loadFragment(new MapViewFragment());

                ((MainActivity) getActivity()).setDetails(verbindung.getLegs());

                Log.e("fehler", verbindung.getStarthalte() + "   " + String.valueOf(position));
            }
        });

        return root;
    }
    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    public void onBackPressed() {

    }


}